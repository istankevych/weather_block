<?php

/**
 * @file
 * Contains \Drupal\weather_block\Form\NwsWeatherAdminSettings.
 */

namespace Drupal\weather_block\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Component\Utility\UrlHelper;


class WeatherBlockSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_block_settings';
  }

  /**
   * Return a render array for the weather_block block.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('weather_block.settings');
    $form = array();
    $form['lat'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#default_value' => $config->get('weather_block_lat'),
      '#description' => t('The latitude of your location.'),
    );
    $form['lon'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#default_value' => $config->get('weather_block_lon'),
      '#description' => t('The longitude of your location.'),
    );
    $form['api_url'] = array(
      '#type' => 'textfield',
      '#title' => t('API URL'),
      '#default_value' => $config->get('weather_block_api_url'),
      '#description' => t('weather.gov API endpoint URL'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * Checking for numeric values on Longitude and Latitude.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $lat = $form_state->getValue('lat');
    if (!is_numeric($lat) || $lat > 90 || $lat < -90) {
      $form_state->setErrorByName('lat', $this->t('Latitude value must be numeric and between 90 and -90.'));
    }
    $lon = $form_state->getValue('lon');
    if (!is_numeric($lon) || $lon > 180 || $lon < -180) {
      $form_state->setErrorByName('lon', $this->t('Latitude value must be numeric and between 180 and -180.'));
    }
    $api_url = $form_state->getValue('api_url');
    if (!UrlHelper::isValid($api_url, TRUE)) {
      $form_state->setErrorByName('api_url', $this->t('Please use valid API URL.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array(
      'weather_block.settings',
    );
  }

  /**
   * Form submit function.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('weather_block.settings');
    $config->set('weather_block_lat', $form_state->getValue('lat'))
      ->set('weather_block_lon', $form_state->getValue('lon'))
      ->set('weather_block_api_url', $form_state->getValue('api_url'));
    $config->save();
    parent::submitForm($form, $form_state);
  }
}

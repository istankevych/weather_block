<?php

namespace Drupal\weather_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'WeatherBlock' block.
 *
 * @Block(
 *  id = "weather_block",
 *  admin_label = @Translation("Weather block"),
 * )
 */
class WeatherBlock extends BlockBase {


  public function defaultConfiguration() {
    return [
      'block_type' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['block_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Block type'),
      '#options' => [
        0 => 'Current weather',
        1 => 'Weather forecast',
      ],
      '#default_value' => $this->configuration['block_type'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_type']
      = $form_state->getValue('block_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    if ($data = weather_block_get_data()) {
      $build['#theme'] = 'weather_block';
      $build['#attached'] = [
        'library' => [
          'weather_block/weather_theme',
        ],
      ];
      $build['#city'] = $data['city'];
      $build['#state'] = $data['state'];
      $build['#periods'] = $this->configuration['block_type'] ? $data['periods'] : [reset($data['periods'])];
    }
    else {
      $build['output'] = [
        '#markup' => \Drupal::translation()
          ->translate('Can not get weather data.'),
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 3600;
  }

}
